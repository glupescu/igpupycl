from __future__ import absolute_import
from __future__ import print_function
# example by Roger Pau Monn'e
import pyopencl as cl
import numpy as np
import pyopencl.array as cl_array
import time
import gc

def prepareKernel():

    kernelMatMulFile = open("matmut.cl", "r")
    kernelMatMulStr = kernelMatMulFile.read()

    ctx = cl.create_some_context()
    queue = cl.CommandQueue(ctx)

    prg = cl.Program(ctx, kernelMatMulStr)

    try:
        prg.build()
    except:
        print("Error:")
        print(prg.get_build_info(ctx.devices[0], cl.program_build_info.LOG))
        raise

    return ctx, queue, prg

def main():

    # get cl objects
    ctx, queue, prg = prepareKernel()

    # generate random numbers
    matSize = 1000
    print('Matrix dimension: ', matSize)

    matA = np.random.rand(matSize, matSize).astype('f')
    matB = np.random.rand(matSize, matSize).astype('f')
    matAflat = matA.flatten()
    matBT = matB.transpose()
    matBflatT = matBT.flatten()
    matCflat = matA.flatten()

    gc.disable()
    # CPU verification
    print('CPU compute start...')
    startTime = time.time()
    for i in range(10):
        matABResult = np.dot(matA, matB)
    elapsedTime = (time.time() - startTime) / 10
    print('CPU compute numpy: {} ms'.format(int(elapsedTime * 1000)))
    print('CPU performance: {} mflops'.format(int(matSize * matSize * 2 / elapsedTime / pow(10,6))))
    matABflat = matABResult.flatten()

    # GPU start
    print('GPU setup...')
    mf = cl.mem_flags

    #gpumatA = cl_array.to_device(queue, matAflat)
    #gpumatB = cl_array.to_device(queue, matBflat)
    #gpumatC = cl_array.empty_like(gpumatA)

    gpumatA = cl.Buffer(ctx, mf.READ_WRITE, matAflat.nbytes)
    gpumatB = cl.Buffer(ctx, mf.READ_WRITE, matBflatT.nbytes)
    gpumatC = cl.Buffer(ctx, mf.READ_WRITE, matCflat.nbytes)

    startTime = time.time()
    cl._enqueue_write_buffer(queue, gpumatA, matAflat).wait()
    cl._enqueue_write_buffer(queue, gpumatB, matBflatT).wait()
    elapsedTime = time.time() - startTime
    print('GPU RPCIe performance: {} MB/sec'.format(int(2*matSize * matSize / elapsedTime / pow(10,6))))

    print('GPU compute start...')

    # GPU processing end
    startTime = time.time()
    for i in range(10):
        prg.mat_mul(queue, (matSize, matSize), None,
                    gpumatA, gpumatB, gpumatC, np.int32(matSize)).wait()
    elapsedTime = (time.time() - startTime) / 10
    print('GPU compute: {} ms'.format(int(elapsedTime * 1000)))
    print('GPU performance: {} mflops'.format(int(matSize * matSize * 2 / elapsedTime/ pow(10,6))))

    startTime = time.time()
    #cl.enqueue_copy(queue, matCflat, gpumatC)
    cl._enqueue_read_buffer(queue, gpumatC, matCflat).wait()
    elapsedTime = time.time() - startTime
    print('GPU WPCIe performance: {} MB/sec'.format(int(matSize * matSize / elapsedTime/ pow(10,6))))

    if np.allclose(matABflat, matCflat):
        print("Equality check OK")
    else:
        print("Equality check ERROR")

    if matSize < 16:
        print(matAflat)
        print(matBflatT)
        print(matABflat)
        print(matCflat)

if __name__ == "__main__":
    main()

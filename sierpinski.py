from __future__ import absolute_import
from __future__ import print_function
# example by Roger Pau Monn'e
import pyopencl as cl
import numpy as np
import time


def prepareKernel():

    kernelSierpinskiFile = open("sierpinski.cl", "r")
    kernelSierpinskiStr = kernelSierpinskiFile.read()

    ctx = cl.create_some_context()
    queue = cl.CommandQueueFull(ctx)
    #queue = cl.CommandQueue(ctx)

    prg = cl.Program(ctx, kernelSierpinskiStr)

    try:
        prg.build(options=["-cl-std=CL2.0"])
    except:
        print("Error:")
        print(prg.get_build_info(ctx.devices[0], cl.program_build_info.LOG))
        raise

    return ctx, queue, prg

def sierpinski_IGPU(img_hsize, img_vsize, ctx, queue, prg):


    img_sierpinski = np.empty(img_hsize * img_vsize, dtype=np.byte)

    mf = cl.mem_flags
    img_gpu = cl.Buffer(ctx, mf.READ_WRITE, img_sierpinski.nbytes)

    startTime_igpu = time.time()
    prg.sierpinski(queue, (img_hsize, img_vsize), None, img_gpu, np.int32(img_vsize), np.int32(0), np.int32(0))
    cl.enqueue_read_buffer(queue, img_gpu, img_sierpinski).wait()
    elapsedTime_igpu = time.time() - startTime_igpu

    return img_sierpinski

def main():

    # get cl objects
    ctx, queue, prg = prepareKernel()

    # image settings
    img_hsize = 2187
    img_vsize = 2187

    # generate on iGPU
    startTime = time.time()
    img_sierpinski = sierpinski_IGPU(img_hsize, img_vsize, ctx, queue, prg)
    elapsedTime = time.time() - startTime
    print('sierpinski time: {} ms'.format(int(elapsedTime * 1000)))

    ppmfile = open("picture.ppm", 'wb+')  # note the binary flag
    ppmfile.write("P2\n")
    ppmfile.write("#LG\n")
    ppmfile.write("%d %d\n" % (img_vsize, img_hsize))
    ppmfile.write("255\n")

    for val in img_sierpinski:
        ppmfile.write(val)

    ppmfile.close()

if __name__ == "__main__":
    main()

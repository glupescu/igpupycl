from __future__ import absolute_import
from __future__ import print_function
# example by Roger Pau Monn'e
import pyopencl as cl
import numpy as np
import time


def prepareKernel():

    kernelSortFile = open("hsort.cl", "r")
    kernelSortStr = kernelSortFile.read()

    ctx = cl.create_some_context()
    queue = cl.CommandQueue(ctx)

    prg = cl.Program(ctx, kernelSortStr)

    try:
        prg.build()
    except:
        print("Error:")
        print(prg.get_build_info(ctx.devices[0], cl.program_build_info.LOG))
        raise

    return ctx, queue, prg


def sortIGPU_full(vec, ctx, queue, prg):
    mf = cl.mem_flags
    vec_gpu = cl.Buffer(ctx, mf.READ_WRITE, vec.nbytes)
    vec_size = vec.shape[0]
    vec_sorted = np.empty(vec_size, dtype=np.float32)

    cl._enqueue_write_buffer(queue, vec_gpu, vec).wait()
    
    #prg.demo(queue, (50,), None, vec_gpu)

    i = 2
    while (i <= vec_size):
        j = i >> 1
        while (j > 0):
            prg.bitonic_sort(queue, (vec_size, ), None, vec_gpu, np.uint32(j), np.uint32(i))
            j = j >> 1
        i = i << 1

    cl._enqueue_read_buffer(queue, vec_gpu, vec_sorted).wait()
    return vec_sorted

def sortIGPU_hybrid(vec, ctx, queue, prg):

    chunkSize = 4096
    localSize = 128
    elemPerWorkItem = 32
    globalSize = vec.shape[0] / elemPerWorkItem

    mf = cl.mem_flags
    vec_gpu = cl.Buffer(ctx, mf.READ_WRITE, vec.nbytes)
    vec_size = vec.shape[0]
    vec_sorted = np.empty(vec_size, dtype=np.float32)

    startTime_igpu = time.time()
    cl._enqueue_write_buffer(queue, vec_gpu, vec).wait()
    prg.bitonic_sort_lds(queue, (globalSize, ), (localSize, ), vec_gpu, np.int32(elemPerWorkItem))
    cl._enqueue_read_buffer(queue, vec_gpu, vec_sorted).wait()
    elapsedTime_igpu = time.time() - startTime_igpu

    startTime_cpu = time.time()
    vec_sorted = sorted(vec_sorted)
    elapsedTime_cpu = time.time() - startTime_cpu
    print('sortIGPU_hybrid sort time: iGPU {} + CPU {} ms'.format(
        int(elapsedTime_igpu * 1000), int(elapsedTime_cpu * 1000)))

    return vec_sorted

def main():

    # get cl objects
    ctx, queue, prg = prepareKernel()

    # generate random numbers
    demo_r = np.random.rand(4096 * 1024).astype('f')

    isHybridSort = False

    if isHybridSort:
        # sort hybrid CPU-iGPU
        startTime = time.time()
        demo_sorted_igpu = sortIGPU_hybrid(demo_r, ctx, queue, prg)
        elapsedTime = time.time() - startTime
        print('sortIGPU_hybrid sort time: {} ms'.format(int(elapsedTime * 1000)))
    else:
        # sort full iGPU
        startTime = time.time()
        demo_sorted_igpu = sortIGPU_full(demo_r, ctx, queue, prg)
        elapsedTime = time.time() - startTime
        print('sortIGPU_full sort time: {} ms'.format(int(elapsedTime * 1000)))

    # sort only on CPU
    startTime = time.time()
    demo_sorted_py = sorted(demo_r)
    elapsedTime = time.time() - startTime
    print('CPU sort time: {} ms'.format(int(elapsedTime * 1000)))

    print("---- CPU Py sort ----")
    for res in demo_sorted_py[:10]:
        print(res)

    print("---- iGPU sort ----")
    for res in demo_sorted_igpu[:10]:
        print(res)

if __name__ == "__main__":
    main()

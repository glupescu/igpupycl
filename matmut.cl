/* plain matmul */
__kernel void mat_mul(__global float* matA,
    __global float* matB,
    __global float* matC,
    int matSize) {
    uint gidX = get_global_id(0);
    uint gidY = get_global_id(1);

    float result = 0.f;
    for(int i = 0; i < matSize; i++) {
        result = fma(matA[gidY * matSize + i], matB[i + gidX * matSize], result);
        //result += matA[gidX][i] * matB[i][gidY];
    }

    matC[gidX + gidY * matSize] = result;
    //matC[gidX][gidY] = result;
}